package plugins.stef.library;

import javax.script.ScriptEngineFactory;
import javax.script.ScriptEngineManager;

import icy.plugin.abstract_.Plugin;
import icy.plugin.interface_.PluginLibrary;
import icy.plugin.interface_.PluginScriptFactory;
import icy.system.SystemUtil;
import icy.util.ClassUtil;
import icy.util.StringUtil;

/**
 * Icy wrapper for the Nashorn Script Engine (not anymore included in Java 15)
 * 
 * @author Stephane Dallongeville
 */
public class NashornPlugin extends Plugin implements PluginLibrary, PluginScriptFactory
{
    @Override
    public ScriptEngineFactory getScriptEngineFactory()
    {
        // Java 11 or above ? --> use the new Nashorn Engine
        if (SystemUtil.getJavaVersionAsNumber() >= 11)
        {
            try
            {
                final Class<?> cl = ClassUtil.findClass("org.openjdk.nashorn.api.scripting.NashornScriptEngineFactory");
                return (ScriptEngineFactory) cl.newInstance();
            }
            catch (Exception e)
            {
                System.err.println(e.getMessage());
                return null;
            }
        }

        // Java < 11 --> try to use the internal JavaScript engine
        for (ScriptEngineFactory sef : new ScriptEngineManager().getEngineFactories())
            for (String ext : sef.getExtensions())
                if (StringUtil.equals(ext.toLowerCase(), "js"))
                    return sef;

        return null;
    }
}
